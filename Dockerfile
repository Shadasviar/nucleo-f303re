FROM ubuntu:20.04

ARG UID=1000
ARG USERNAME=root
ARG WORKDIR=/home/$USERNAME
RUN useradd -d /home/$USERNAME -ms /bin/bash -g root -G sudo -u $UID $USERNAME || true
RUN mkdir -p $WORKDIR

VOLUME $WORKDIR
WORKDIR $WORKDIR

ENV DEBIAN_FRONTEND=noninteractive
RUN apt update -y && apt install -y make cmake g++ doxygen graphviz \
  binutils-arm-none-eabi gcc-arm-none-eabi libnewlib-arm-none-eabi \
  libstdc++-arm-none-eabi-newlib git

USER $USERNAME
