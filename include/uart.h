#ifndef UART_H
#define UART_H

#include <stdint.h>
#include <stm32f3xx_hal.h>
#include <stm32f3xx_hal_conf.h>

/**
 * @brief Initialize UART.
 */
void uartInit();

/**
 * @brief Send data to UART.
 * @param data Pointer to data buffer to send.
 * @param size Size od data buffer to send in bytes.
 */
void uartSend(uint8_t* data, const uint16_t size);

/**
 * @brief Receive data from UART.
 * @param data Pointer to data buffer to read data to.
 * @param size Size of data to be read in bytes.
 */
void uardRecieve(uint8_t* data, const uint16_t size);

/**
 * @brief Overriden implementation of GCC _write function, which allow to use
 * printf and scanf to UART.
 * @param file File descriptor to write data to. Unused.
 * @param ptr Pointer to data buffer to write to UART.
 * @param len Size of data to write to UART, in bytes
 * (number of characters to send).
 * @return 0 on success.
 */
int _write(int file, char* ptr, int len);

#endif /*UART_H*/
