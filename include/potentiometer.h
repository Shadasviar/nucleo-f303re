#ifndef POTENTIOMETER_H
#define POTENTIOMETER_H

#include <stdint.h>

/**
 * @brief Actual potentiometer position in range [0-1000]
 * @return Potentiometer resistance from 0 to 1000.
 */
uint16_t potentiometerPosition();

/**
 * @brief Initialize potentiometer.
 */
void potentiometerInit();

#endif /*POTENTIOMETER_H*/
