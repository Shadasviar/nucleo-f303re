#ifndef SERVO_H
#define SERVO_H

#include <stdint.h>
#include <stdbool.h>

#include <stm32f3xx_hal.h>
#include <stm32f3xx_hal_conf.h>

/**
 * @brief Move servo to given position [1000-2000].
 * @param pos Servo PWM duty.
 * @return true ob success, otherwise false.
 */
bool servoMove(const uint16_t pos);

/**
 * @brief Initialize servo.
 */
void servoInit();

#endif /*SERVO_H*/
