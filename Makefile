all:
	cmake -Bbuild .
	${MAKE} -C build

clean:
	rm -rf build

ROOT_DIR=$(shell pwd)
BUILD_IMAGE_NAME=nucleo_build

.docker-build:
	docker build \
		--build-arg UID=$(shell id -u) \
		--build-arg USERNAME=${USER} \
		--build-arg WORKDIR=${PWD} \
		--tag ${BUILD_IMAGE_NAME} .
	@touch $@

docker-clean:
	docker rmi -f ${BUILD_IMAGE_NAME} || true
	rm -f .docker-build

docker-build: .docker-build
	docker run -it \
		-v ${ROOT_DIR}:${ROOT_DIR} \
		${BUILD_IMAGE_NAME} ${MAKE}

flash:
	${MAKE} -C build flash
