############################## Make version header ###########################
CONFIGURE_FILE("${CMAKE_MODULES}/version.h.in" "version.h")

#################### Generated header located at build tree ##################
TARGET_INCLUDE_DIRECTORIES(${TARGET} PUBLIC "${PROJECT_BINARY_DIR}")
