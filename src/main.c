#include <stm32f3xx_hal.h>
#include <stm32f3xx_hal_conf.h>
#include <FreeRTOS.h>
#include <task.h>

#include <potentiometer.h>
#include <servo.h>
#include <uart.h>

#include <stdio.h>

void default_task(void* arg) {
  UNUSED(arg);
  GPIO_InitTypeDef gpio;
  gpio.Pin    = GPIO_PIN_5;
  gpio.Mode   = GPIO_MODE_OUTPUT_PP;
  gpio.Pull   = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOA, &gpio);

  while (1) {
    HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);
    vTaskDelay(500 / portTICK_PERIOD_MS);
  }

}

void servo_task(void* arg) {
  UNUSED(arg);
  while (1) {
    servoMove(potentiometerPosition() + 1000);
    vTaskDelay(20 / portTICK_PERIOD_MS);
  }
}

void uart_task(void* arg) {
  UNUSED(arg);

  while (1) {
    printf("Position = %d\n", potentiometerPosition());
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
}

/**
 * @brief Program entry point.
 */
int main() {

  HAL_Init();
  potentiometerInit();
  servoInit();
  uartInit();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  xTaskCreate(default_task, "default_task", 128, NULL, tskIDLE_PRIORITY+2, NULL);
  xTaskCreate(servo_task, "servo_task", 128, NULL, tskIDLE_PRIORITY+1, NULL);
  xTaskCreate(uart_task, "uart_task", 128, NULL, tskIDLE_PRIORITY+3, NULL);

  vTaskStartScheduler();

  while (1) {
  }

  return 0;
}
