#include <potentiometer.h>

#include <stm32f3xx_hal.h>
#include <stm32f3xx_hal_conf.h>

#define ADC_PORT          GPIOA
#define ADC_PIN           GPIO_PIN_0
#define ADC_GPIO_ENABLE   __HAL_RCC_GPIOA_CLK_ENABLE
#define ADC_CHANNEL       ADC_CHANNEL_1

static ADC_HandleTypeDef hadc = {
  .Instance               = ADC1,
  .Init.Resolution        = ADC_RESOLUTION_10B,
  .Init.ClockPrescaler    = ADC_CLOCK_SYNC_PCLK_DIV1,
  .Init.EOCSelection      = ADC_EOC_SINGLE_CONV,
  .Init.ExternalTrigConv  = ADC_SOFTWARE_START,
};

uint16_t potentiometerPosition() {
  HAL_ADC_Start(&hadc);
  HAL_ADC_PollForConversion(&hadc, 10000);
  uint16_t res = HAL_ADC_GetValue(&hadc);
  HAL_ADC_Stop(&hadc);
  return res;
}

void HAL_ADC_MspInit(ADC_HandleTypeDef* h) {
  if (&hadc == h) {
    __HAL_RCC_ADC1_CLK_ENABLE();
    ADC_GPIO_ENABLE();

    GPIO_InitTypeDef pin = {
      .Pin = ADC_PIN,
      .Mode = GPIO_MODE_INPUT,
    };
    HAL_GPIO_Init(ADC_PORT, &pin);
  }
}

void potentiometerInit() {

  HAL_ADC_Init(&hadc);

  ADC_ChannelConfTypeDef channel = {
    .Channel      = ADC_CHANNEL,
    .Rank         = ADC_REGULAR_RANK_1,
    .SamplingTime = ADC_SAMPLETIME_1CYCLE_5,
    .SingleDiff   = ADC_SINGLE_ENDED,
  };

  HAL_ADC_ConfigChannel(&hadc, &channel);
  HAL_ADCEx_Calibration_Start(&hadc, ADC_SINGLE_ENDED);
}
