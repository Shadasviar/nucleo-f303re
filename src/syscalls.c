#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#define UNUSED __attribute__((unused))

__attribute__((weak)) int _close(UNUSED int fd) {
  return 0;
}

__attribute__((weak, noreturn)) void _exit(UNUSED int status) {
  while (1);
}

__attribute__((weak)) off_t _lseek(UNUSED int fd, UNUSED off_t offset, UNUSED int whence) {
  return -1;
}

__attribute__((weak)) size_t _read(UNUSED int fd, UNUSED void* buf, UNUSED size_t count) {
  return 0;
}

__attribute__((weak)) int _isatty(UNUSED int fd) {
  return 0;
}

__attribute__((weak)) int _fstat(UNUSED int fd, UNUSED struct stat* buf) {
  return 0;
}
