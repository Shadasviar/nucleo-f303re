#include <servo.h>

#include <stm32f3xx_hal.h>
#include <stm32f3xx_hal_conf.h>

#include <stdio.h>

#define SERVO_PORT            GPIOB
#define SERVO_PIN             GPIO_PIN_10
#define SERVO_TIM             TIM2
#define SERVO_CHANNEL         TIM_CHANNEL_3
#define SERVO_TIM_ENABLE()    __HAL_RCC_TIM2_CLK_ENABLE()
#define SERVO_GPIO_ENABLE()   __HAL_RCC_GPIOB_CLK_ENABLE()

static TIM_HandleTypeDef htim = {
  .Instance               = SERVO_TIM,
  .Init.Prescaler         = 8,
  .Init.Period            = 20000,
  .Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE,
  .Init.CounterMode       = TIM_COUNTERMODE_UP,
};

bool servoMove(const uint16_t pos) {
  if (pos < 1000 || pos > 2000) return false;
  __HAL_TIM_SET_COMPARE(&htim, SERVO_CHANNEL, pos);
  return true;
}

void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef* h) {
  if (h == &htim) {
    SERVO_TIM_ENABLE();
    SERVO_GPIO_ENABLE();

    GPIO_InitTypeDef pin = {
      .Pin        = SERVO_PIN,
      .Mode       = GPIO_MODE_AF_PP,
      .Alternate  = GPIO_AF1_TIM2,
      .Speed      = GPIO_SPEED_FREQ_HIGH,
      .Pull       = GPIO_NOPULL,
    };
    HAL_GPIO_Init(SERVO_PORT, &pin);
    HAL_GPIO_WritePin(SERVO_PORT, SERVO_PIN, GPIO_PIN_SET);
  }
}

void servoInit() {
  HAL_TIM_PWM_Init(&htim);

  TIM_OC_InitTypeDef channel = {
    .OCMode       = TIM_OCMODE_PWM1,
    .OCPolarity   = TIM_OCPOLARITY_HIGH,
    .OCNPolarity  = TIM_OCNPOLARITY_LOW,
    .OCIdleState  = TIM_OCIDLESTATE_SET,
    .OCNIdleState = TIM_OCNIDLESTATE_RESET,
    .OCFastMode   = TIM_OCFAST_ENABLE,
  };
  HAL_TIM_PWM_ConfigChannel(&htim, &channel, SERVO_CHANNEL);
  HAL_TIM_PWM_Start(&htim, SERVO_CHANNEL);
}

