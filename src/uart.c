#include <uart.h>

#include <stm32f3xx_hal.h>
#include <stm32f3xx_hal_conf.h>

#include <string.h>

#define UART_RX_PORT  GPIOC
#define UART_RX_PIN   GPIO_PIN_11
#define UART_RX_AF    GPIO_AF5_UART4

#define UART_TX_PORT  GPIOC
#define UART_TX_PIN   GPIO_PIN_10
#define UART_TX_AF    GPIO_AF5_UART4

#define UART_INSTANCE           UART4
#define UART_GPIO_CLK_ENABLE()  __HAL_RCC_GPIOC_CLK_ENABLE()
#define UART_CLK_ENABLE()       __HAL_RCC_UART4_CLK_ENABLE()
#define UART_IRQ                UART4_IRQn
#define UART_IRQ_HANDLER        UART4_IRQHandler

static UART_HandleTypeDef huart = {
  .Instance         = UART_INSTANCE,
  .Init.BaudRate    = 57600,
  .Init.WordLength  = UART_WORDLENGTH_8B,
  .Init.StopBits    = UART_STOPBITS_1,
  .Init.Mode        = UART_MODE_TX_RX,
};

void HAL_UART_MspInit(UART_HandleTypeDef* h) {
  if (h == &huart) {
    UART_GPIO_CLK_ENABLE();
    UART_CLK_ENABLE();

    GPIO_InitTypeDef pin = {
      .Pin        = UART_RX_PIN,
      .Mode       = GPIO_MODE_AF_PP,
      .Pull       = GPIO_NOPULL,
      .Alternate  = UART_RX_AF,
    };
    HAL_GPIO_Init(UART_RX_PORT, &pin);

    pin.Pin = UART_TX_PIN;
    pin.Alternate = UART_TX_AF;
    HAL_GPIO_Init(UART_TX_PORT, &pin);
  }
}

void uartInit() {
  HAL_UART_Init(&huart);
  HAL_NVIC_SetPriority(UART_IRQ, 2, 2);
  HAL_NVIC_EnableIRQ(UART_IRQ);
}

void UART_IRQ_HANDLER() {
  HAL_UART_IRQHandler(&huart);
}

void uartSend(uint8_t* data, const uint16_t size) {
  HAL_UART_Transmit(&huart, data, size, HAL_MAX_DELAY);
}

int _write(int file, char* ptr, int len) {
  (void)file;
  uartSend((uint8_t*)ptr, len);
  return 0;
}

void uardRecieve(uint8_t* data, const uint16_t size) {
  HAL_UART_Receive_IT(&huart, data, size);
}
