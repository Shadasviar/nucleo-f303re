#include "stm32f3xx_hal.h"
#include "stm32f3xx_hal_tim.h"

#define HAL_TIMEBASE_TIM            TIM3
#define HAL_TIMEBASE_TIM_ENABLE     __HAL_RCC_TIM3_CLK_ENABLE
#define HAL_TIMEBASE_IRQ            TIM3_IRQn
#define HAL_TIMEBASE_IRQ_HANDLER    TIM3_IRQHandler

static TIM_HandleTypeDef TIM_Handler = {0};

void HAL_TIMEBASE_IRQ_HANDLER() {
  HAL_TIM_IRQHandler(&TIM_Handler);
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when HAL_TIMEBASE_TIM interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  if (htim->Instance == HAL_TIMEBASE_TIM) {
    HAL_IncTick();
  }
}

/**
  * @brief  This function configures the HAL_TIMEBASE_TIM as a time base source.
  *         The time source is configured  to have 1ms time base with a dedicated
  *         Tick interrupt priority.
  * @note   This function is called  automatically at the beginning of program after
  *         reset by HAL_Init() or at any time when clock is configured, by HAL_RCC_ClockConfig().
  * @param  TickPriority: Tick interrupt priority.
  * @retval HAL status
  */
HAL_StatusTypeDef HAL_InitTick(uint32_t TickPriority)
{
  RCC_ClkInitTypeDef    clkconfig;
  uint32_t              uwTimclock = 0;
  uint32_t              uwPrescalerValue = 0;
  uint32_t              pFLatency;

  HAL_NVIC_SetPriority(HAL_TIMEBASE_IRQ, TickPriority ,0);
  HAL_NVIC_EnableIRQ(HAL_TIMEBASE_IRQ);

  HAL_TIMEBASE_TIM_ENABLE();

  HAL_RCC_GetClockConfig(&clkconfig, &pFLatency);

  uwTimclock = HAL_RCC_GetPCLK1Freq();
  uwPrescalerValue = (uint32_t) ((uwTimclock / 1000000) - 1);

  TIM_Handler.Instance = HAL_TIMEBASE_TIM;

  /* Initialize TIMx peripheral as follow:
  + Period = [(TIM2CLK/1000) - 1]. to have a (1/1000) s time base.
  + Prescaler = (uwTimclock/1000000 - 1) to have a 1MHz counter clock.
  + ClockDivision = 0
  + Counter direction = Up
  */
  TIM_Handler.Init.Period = (1000000 / 1000) - 1;
  TIM_Handler.Init.Prescaler = uwPrescalerValue;
  TIM_Handler.Init.ClockDivision = 0;
  TIM_Handler.Init.CounterMode = TIM_COUNTERMODE_UP;
  if(HAL_TIM_Base_Init(&TIM_Handler) == HAL_OK)
  {
    return HAL_TIM_Base_Start_IT(&TIM_Handler);
  }

  return HAL_ERROR;
}

/**
  * @brief  Suspend Tick increment.
  * @note   Disable the tick increment by disabling TIM2 update interrupt.
  * @param  None
  * @retval None
  */
void HAL_SuspendTick(void)
{
  /* Disable TIM2 update Interrupt */
  __HAL_TIM_DISABLE_IT(&TIM_Handler, TIM_IT_UPDATE);
}

/**
  * @brief  Resume Tick increment.
  * @note   Enable the tick increment by Enabling TIM2 update interrupt.
  * @param  None
  * @retval None
  */
void HAL_ResumeTick(void)
{
  /* Enable TIM2 Update interrupt */
  __HAL_TIM_ENABLE_IT(&TIM_Handler, TIM_IT_UPDATE);
}
